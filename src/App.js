import * as React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { createTheme, ThemeProvider } from "@mui/material";
import Theme from "../src/theme/theme";
import SignInPage from "../src/signIn/index";
import NewProperty from "./newProperty";
import PropertyList from "./propertyList";
import Layout from "./components/layout/layout";
import { Box } from "@mui/system";

function App() {
  const theme = createTheme(Theme());
  return (
    <>
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<SignInPage />} />
            <Route path='layout' element={<Layout />}>
            <Route
              path="newProperty"
              element={
                <Box mt={8} ml={8}>
                  <NewProperty />
                </Box>
              }
            />
            <Route
              path="propertyList"
              element={
                <Box mt={8} ml={8}>
                  <PropertyList />
                </Box>
              }
            />
            </Route>
          </Routes>
        </BrowserRouter>
      </ThemeProvider>
    </>
  );
}

export default App;
