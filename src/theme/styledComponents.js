import { styled } from '@mui/material/styles';
import { TextField, Button, Select} from '@mui/material';

export const CustomTextField = styled(TextField)({
    '& .MuiOutlinedInput-root': {
        '& fieldset': {
            padding: '15px 12px',
            border: '1px solid #E4E8EE',
            borderRadius: 10,
        },
        // '&:hover fieldset': {
        //     border: '1px solid #E4E8EE'
        // },
        // '&.Mui-focused fieldset': {
        //     border: '1px solid #5078E1'
        // },
        '& input:valid + filedset': {
            border: '1px solid #5078E1'
        },
        '& input:invalid + fieldset': {
            border: '1px solid red'
        }
    }
})


export const CustomImage = styled('img')({
        display: "block",
        position: 'fixed',
        zIndex: '1',
        left: '-100px',
        bottom: '-100px',
        "@media (max-width: 1024px)": {
            display: "none"
        }
})



export const CustomButton = styled(Button)({
    border: '1px solid #E4E8EE',
    borderRadius: 10,
    color: 'black',
    '&:hover': {
        backgroundColor: 'grey',
        color: 'black'
    }
})

export const CustomSelectField = styled(Select)({
    '& .MuiOutlinedInput-notchedOutline':{
        border: '1px solid #E4E8EE',
        borderRadius: 10,
        color: 'black',
    }
   
})

// export const CustomToggleButton = styled(ToggleButton)({
//     '& .MuiToggleButton-root': {
//         '&: .Mui-selected: {
//             color: "red"
//         },
//         '&:first-of-type': {
//             color: "blue"
//         }
//     }
// })
