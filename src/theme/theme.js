const Theme = () => ({
    palette: {
        primary: {
            main: "#5078E1",
            light: "#ffffff"
        }
    },
    components: {
        MuiButtonBase: {
            defaultProps: {
                disableRipple: false,
            },
        },
        MuiInputBase: {
            styleOverrides: {
                input: {
                    fontSize: 14,
                    color: '#091B29',
                    padding: '12px 15px !important',
                },
            }
        },
        MuiButton: {
            styleOverrides: {
              root: ({ ownerState }) => ({
                ...(ownerState.variant === 'contained' &&
                  ownerState.color === 'primary' && {
                    backgroundColor: '#5078E1',
                    color: '#ffffff',
                  }),
              }),
            },
          },
    },

   

    typography: {
        label: {
            display: 'inline-block',
            marginBottom: '7px',
            fontFamily: 'Nunito Sans, sans-serif',
            fontSize: 12,
            fontWeight: 600,
            color: '#98A0AC',
        },
        h5: {
            fontFamily: 'Nunito Sans, sans-serif',
            fontWeight: 800,
            marginBottom: "15px" 
        },
        h6: {
            fontFamily: 'Nunito Sans, sans-serif',
            fontWeight: 600,
            fontSize: 12,
            textAlign: "right",
            margin: 0
        },
        subtitle1: {
            fontSize: 12,
            fontWeight: 'bold',
            color: 'white'
        },
        subtitle2: {
            fontSize: 16,
            color: "#091B29",
            fontWeight: "bold",
            marginLeft: '13px'
        }, 
        overline: {
            fontSize: '12px',
            color: '#4E5A6B',
            fontWeight: 'bold'
        },
        button: {
            textTransform: "none",
            fontSize: '12px'
        }
    }
})

export default Theme;