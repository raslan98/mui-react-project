import * as React from "react";
import { Box, Typography, Avatar, Popper, Paper } from "@mui/material";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import Logo from "../../../assets/property-logo.png";

export default function Header() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popper" : undefined;

  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: 1,
        backgroundColor: "#1C1C1C",
        color: "white",
      }}
    >
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          flex: 1,
        }}
      >
        <Box mr={1}>
          <img src={Logo} alt="logo" width="150" />
        </Box>
        <span>|</span>
        <Box ml={1} sx={{ fontSize: "10px", fontWeight: 600 }}>
          PROPERTY MANAGEMENT SOLUTION
        </Box>
      </Box>
      <Box
        sx={{
          display: "flex",
          flex: 1,
          justifyContent: "right",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <Box mr={1}>
          <NotificationsNoneIcon />
        </Box>
        <span>|</span>
        <Box display="flex" onClick={handleClick}>
          <Box ml={1} mr={1}>
            <Avatar>H</Avatar>
          </Box>
          <Box ml={0.5} mr={1}>
            <Box display="flex" flexDirection="column">
              <Typography variant="subtitle1">Bala Vignesh</Typography>
              <Typography variant="label">Super admin</Typography>
            </Box>
          </Box>
        </Box>
        <KeyboardArrowDownIcon />
        <Popper id={id} open={open} anchorEl={anchorEl}>
          <Box
            sx={{
              width: "350px",
              margin: 2,
            }}
          >
            <Paper elevation={1}>
              <Box p={1}>
                <Box display="flex">
                  <Box mr={1}>
                    <Avatar>H</Avatar>
                  </Box>
                  <Box ml={0.5} mr={1}>
                    <Box display="flex" flexDirection="column">
                      <Typography variant="subtitle1" sx={{color: 'black'}}>Bala Vignesh</Typography>
                      <Typography variant="label">Super admin</Typography>
                    </Box>
                  </Box>
                </Box>
                <hr color="#98A0AC" />
                <Typography variant="label">Roles</Typography>
              </Box>
            </Paper>
          </Box>
        </Popper>
      </Box>
    </Box>
  );
}
