import * as React from "react";
import MiniDrawer from '../../components/view/drawer';
import { Outlet } from "react-router-dom";

export default function Layout() {
    return(
        <>
        <MiniDrawer />
        <Outlet />
        </>
    )
}