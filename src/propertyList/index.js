import * as React from "react";
import {
  Box,
  Typography,
  Card,
  Paper,
  Grid,
  Pagination,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Skeleton,
} from "@mui/material";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import { CustomTextField } from "../theme/styledComponents";
import FilterAltIcon from "@mui/icons-material/FilterAlt";

export default function PropertyList() {
  
  const [data, setData] = React.useState([]);
  const [loading, setLoading] = React.useState(true);

  const url = "https://638059802f8f56e28ea13db6.mockapi.io/tableData";

  const fetchData = async () => {
    try {
      const response = await fetch(url);
      const jsonData = await response.json();
      setData(jsonData);
      setLoading(false);

    } catch (error) {
      console.log(error);
    }
  };

  React.useEffect(() => {
    fetchData();
  }, []);

  const loader = (
    <>
      {Array.from(Array(5)).map((_, index) => (
          <TableRow>
          <TableCell>
            <Skeleton
              variant="text"
              animation="wave"
              width={150}
              sx={{ fontSize: "1rem" }}
            ></Skeleton>
          </TableCell>
          <TableCell>
            <Skeleton
              variant="text"
              animation="wave"
              width={150}
              sx={{ fontSize: "1rem" }}
            ></Skeleton>
          </TableCell>
          <TableCell>
            <Skeleton
              variant="text"
              animation="wave"
              width={150}
              sx={{ fontSize: "1rem" }}
            ></Skeleton>
          </TableCell>
          <TableCell>
            <Skeleton
              variant="text"
              animation="wave"
              width={150}
              sx={{ fontSize: "1rem" }}
            ></Skeleton>
          </TableCell>
          <TableCell>
            <Skeleton
              variant="text"
              animation="wave"
              width={150}
              sx={{ fontSize: "1rem" }}
            ></Skeleton>
          </TableCell>
          <TableCell>
            <Skeleton
              variant="text"
              animation="wave"
              width={150}
              sx={{ fontSize: "1rem" }}
            ></Skeleton>
          </TableCell>
          <TableCell>
            <Skeleton
              variant="text"
              animation="wave"
              width={150}
              sx={{ fontSize: "1rem" }}
            ></Skeleton>
          </TableCell>
          </TableRow>
      ))}
      </>
  );

  const maping = (
    <>
      {data.map((row) => (
        <TableRow
          key={row.pname}
          sx={{
            "&:last-child td, &:last-child th": {
              border: 0,
              maringBottom: "150px",
            },
          }}
        >
          <TableCell component="th" scope="row">
            {row.number}
          </TableCell>
          <TableCell>{row.name}</TableCell>
          <TableCell>{row.company}</TableCell>
          <TableCell>{row.location}</TableCell>
          <TableCell>{row.revenue}</TableCell>
          <TableCell>{row.property}</TableCell>
          <TableCell>{row.status}</TableCell>
        </TableRow>
      ))}
    </>
  );

  return (
    <Box sx={{ backgroundColor: "#F5F7FA", height: "92vh" }}>
      <Card>
        <Box
          sx={{ display: "flex", alignItems: "center", padding: "10px 20px" }}
        >
          <ArrowBackIosNewIcon fontSize="big" />
          <Typography variant="subtitle2">Properties</Typography>
        </Box>
      </Card>
      <Grid container spacing={2} mt={3} ml={2} pr={6}>
        <TableContainer component={Paper} sx={{ padding: "20px" }}>
          <Box display="flex" alignItems="center" mb={3} mr={2}>
            <Box flex="1">
              <CustomTextField placeholder="Search properties" />
            </Box>
            <Box flex="1" textAlign="right">
              <FilterAltIcon />
            </Box>
          </Box>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead sx={{ backgroundColor: "#F2F4F7" }}>
              <TableRow>
                <TableCell>Property Num</TableCell>
                <TableCell>Property Name</TableCell>
                <TableCell>Company Name</TableCell>
                <TableCell>Location</TableCell>
                <TableCell>Revenue Type</TableCell>
                <TableCell>Property Type</TableCell>
                <TableCell>Status</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{loading ? loader : maping}</TableBody>
          </Table>
          <Stack spacing={2} mt={10}>
            <Pagination count={10} />
          </Stack>
        </TableContainer>
      </Grid>
    </Box>
  );
}
