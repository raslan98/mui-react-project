import * as React from "react";
import { useNavigate } from "react-router-dom";
import { Grid, Box, useTheme, Typography, Button } from "@mui/material";
import { SignInPageStyles } from "./style";
import { CustomTextField, CustomImage } from "../theme/styledComponents";
import { validateEmail } from '../utils/helper';
import Logo from "../assets/logo.png";
import Building from "../assets/building.png";
// import { BackgroundImg } from "./style";

export default function SignInPage() {
  const classes = SignInPageStyles(useTheme());
  // const useStyles = BackgroundImg();
  const navigate = useNavigate();
  const [username, Setusername] = React.useState("");
  const [password, Setpassword] = React.useState("");
  const [error, setError] = React.useState({username: "", password: ""})

  function change() {
    setError({username: "", password: ""});
    if (!validateEmail(username)){
      setError({ ...error, username: "* Invalid username"});
    } 
    else if (password.trim() === "") {
      setError({...error, password: "* Invalid password"});
    } else {
      navigate("/layout/newProperty");
    }
  }

  

  return (
    <Grid container sx={{ overflowY: "hidden" }}>
      <Grid item lg={3} md={0} sm={0} xs={0}>
        <Box sx={classes.whiteBackground}>
          <CustomImage src={Building} alt="buliding" />
        </Box>
      </Grid>
      <Grid item lg={9} md={12} sm={12} xs={12}>
      <Box sx={classes.blueBackground}>
        <Grid container justifyContent="center">
          <Grid item lg={6}>
          </Grid>
          <Grid item lg={6}>
          <Box sx={classes.signInBox} my={"30%"}>
            <Typography variant="h5">Sign In</Typography>
            <Box mt={4} mb={2}>
              <Typography variant="label">Mobile Number/ Email Id</Typography>
              <CustomTextField
                fullWidth
                variant="outlined"
                name="phoneNumber"
                type="email"
                placeholder="Enter Your Mobile Number / Email Id"
                onChange={(e) => Setusername(e.target.value)}
              ></CustomTextField>
              {username.length === 0 && error.username !== "" &&  <Box><Typography sx={{ color: "red", fontSize: '12px' }}>{error.username}</Typography></Box>}
            </Box>
            <Box mt={3}>
              <Typography variant="label">Password</Typography>
              <CustomTextField
                fullWidth
                variant="outlined"
                name="password"
                type="password"
                placeholder="Enter Your Password"
                onChange={(e) => Setpassword(e.target.value)}
              ></CustomTextField>
              {password.length === 0 && error.password !== "" &&  <Box><Typography sx={{ color: "red", fontSize: "12px" }}>{error.password}</Typography></Box>}
            </Box>
            <Typography variant="h6">
              Did You forget Your Password? <a href="_blank">Click Here</a>
            </Typography>
            <Box mt={6} textAlign="center" sx={classes.logo}>
              <Typography sx={classes.powered}>Powered by</Typography>
              <img src={Logo} alt="logo" />
              <Typography sx={classes.automate}>Property Automtae</Typography>
            </Box>
            <Box mt={2}>
              <Button
                fullWidth
                variant="contained"
                color="primary"
                onClick={change}
              >
                Log In
              </Button>
            </Box>
          </Box>
          </Grid>
        </Grid>
        </Box>
      </Grid>
    </Grid>
  );
}
