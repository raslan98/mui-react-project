export const SignInPageStyles = ({palette, spacing}) => ({
    whiteBackground: {
        height: "100vh",
        backgroundColor: palette.primary.light,
        position: 'relative',
        overflow: 'hidden'
    },
    blueBackground: {
        position: 'relative',
        height: '100vh',
        backgroundColor: palette.primary.main,
    }, 
    signInBox: {
        // position: 'absolute',
        // bottom: spacing(-10),
        // right: '150px',
        backgroundColor: palette.primary.light,
        padding: spacing(4, 4.5),
        borderRadius: spacing(2),
        width:"350px",
        "@media (max-width: 375px)": {
            zoom: '90%'
        },
        "@media (max-width: 320px)": {
            zoom: "75%"
        }                
    },
    logo: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    powered: {
        color: "#98A0AC",
        fontSize: "10px"
    },
    automate: {
        color: "#4E5A6B",
        fontSize: "12px"
    },
    
})