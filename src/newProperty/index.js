import * as React from "react";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import {
  CustomButton,
  CustomTextField,
  CustomSelectField,
} from "../theme/styledComponents";
import {
  Box,
  Typography,
  Paper,
  Grid,
  Card,
  Checkbox,
  MenuItem,
} from "@mui/material";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import Plain from "../../src/assets/plain-building.png";
import { Grey } from "./style";



export default function NewProperty() {
  const grey = Grey();
  const [value, setValue] = React.useState(null);
  const [companyName, setCompanyName] = React.useState("");
  const [propertyName, setPropertyName] = React.useState("")
  const [error, setError] = React.useState({companyName: "", propertyName: ""})

  const [active, setActive] = React.useState(false);
  const handleClick = () => {
    setActive(!active);
  };

  const [active1, setActive1] = React.useState(false);
  const handle = () => {
    setActive1(!active1);
  };

  const [active2, setActive2] = React.useState(false);
  const handleClick2 = () => {
    setActive2(!active2);
  };

  function validate() {
    setError({companyName: "", propertyName: ""})
    if (companyName === "") {
      setError({ ...error, companyName: "* Company Name needs to be mentioned"});
    } else if ( propertyName === "") {
      setError({ ...error, propertyName: "* Property Name needs to be mentioned"});
    }
    else {
      alert ("done")
    }
  }

  return (
    <Box
      sx={{ backgroundColor: "#F5F7FA", height: "100%", overflowX: "hidden" }}
    >
      <Card>
        <Box
          sx={{ display: "flex", alignItems: "center", padding: "10px 20px" }}
        >
          <ArrowBackIosNewIcon fontSize="big" sx={grey.greycolor} />
          <Typography variant="subtitle2">Create New Property</Typography>
        </Box>
      </Card>
      {/* PROPERTY IMAGE  */}
      <Grid container spacing={2} mt={1} ml={0.5} pr={5}>
        <Grid item lg={2} md={12} sm={12} xs={12}>
          <Paper elevation={0} sx={{ borderRadius: 8 }}>
            <Card sx={{ boxShadow: 0 }}>
              <Grid container textAlign="center" py={2}>
                <Grid item lg={12} md={4} sm={12} xs={12}>
                  <Typography variant="overline">PROPERTY IMAGE</Typography>
                </Grid>
                <Grid item lg={12} md={12} sm={12} xs={12}>
                  <img
                    src={Plain}
                    alt="plain Building"
                    width="185px"
                    height="185px"
                  />
                </Grid>
                <Grid item lg={12} md={4} sm={12} xs={12}>
                  <CustomButton
                    variant="outlined"
                    size="medium"
                    component="label"
                  >
                    Upload Image
                    <input hidden accept="image/*" multiple type="file" />
                  </CustomButton>
                </Grid>
              </Grid>
            </Card>
          </Paper>
        </Grid>
        {/* PROPERTY DETAILS */}
        <Grid item lg={10} md={12} sm={12} xs={12}>
          <Paper elevation={0} sx={{ borderRadius: 2 }}>
            <Card sx={{ boxShadow: 0 }}>
              <Grid container p={2}>
                <Grid item lg={12}>
                  <Typography variant="overline">PROPERTY DETAILS</Typography>
                </Grid>
                <Grid item lg={3} md={6} sm={12} xs={12} pr={1}>
                  <Typography variant="label">Company Name</Typography>
                  <CustomSelectField fullWidth onChange={(e) => setCompanyName(e.target.value)}>
                    <MenuItem value={1}>Crayond</MenuItem>
                    <MenuItem value={2}>Crayond1</MenuItem>
                    <MenuItem value={3}>Crayond2</MenuItem>
                  </CustomSelectField>
                  {companyName.length === 0 && error.companyName !== "" &&  <Box><Typography sx={{ color: "red", fontSize: '12px' }}>{error.companyName}</Typography></Box>}
                </Grid>
                <Grid item lg={3} md={6} sm={12} xs={12} pr={1}>
                  <Typography variant="label">Property Name</Typography>
                  <CustomTextField defaultValue="Property Name" required fullWidth onChange={(e) => setPropertyName(e.target.value)}/>
                  {propertyName.length === 0 && error.propertyName !== "" &&  <Box><Typography sx={{ color: "red", fontSize: '12px' }}>{error.propertyName}</Typography></Box>}
                </Grid>
                <Grid item lg={3} md={6} sm={12} xs={12} pr={1}>
                  <Typography variant="label">Payment Period</Typography>
                  <CustomSelectField fullWidth>
                    <MenuItem value={1}>Crayond</MenuItem>
                    <MenuItem value={2}>Crayond1</MenuItem>
                    <MenuItem value={3}>Crayond2</MenuItem>
                  </CustomSelectField>
                </Grid>
                <Grid item lg={3} md={6} sm={12} xs={12} pr={1}>
                  <Typography variant="label">Status</Typography>
                  <CustomSelectField fullWidth>
                    <MenuItem value={1}>Crayond</MenuItem>
                    <MenuItem value={2}>Crayond1</MenuItem>
                    <MenuItem value={3}>Crayond2</MenuItem>
                  </CustomSelectField>
                </Grid>
              </Grid>
              <Grid container p={2}>
                <Grid item lg={12} sm={12}>
                  <Typography variant="label">Property Description</Typography>
                  <ReactQuill />
                </Grid>
              </Grid>
            </Card>
          </Paper>
        </Grid>
      </Grid>
      {/* PROPERTY TYPE */}
      <Grid container spacing={2} ml={0.5} mt={2} pr={5}>
        <Grid item lg={12} md={12} sm={12}>
          <Paper elevation={0} sx={{ borderRadius: 8 }}>
            <Card sx={{ boxShadow: 0 }}>
              <Grid container p={2}>
                <Grid item lg={2} md={2} sm={12} xs={12} pr={1}>
                  <Typography variant="label">Property Type</Typography>
                  <CustomTextField defaultValue="Property Type" required fullWidth />
                </Grid>
                <Grid item lg={2} md={2} sm={12} xs={12} pr={1}>
                  <Typography variant="label">Property Purpose</Typography>
                  <CustomSelectField fullWidth>
                    <MenuItem value={1}>Crayond</MenuItem>
                    <MenuItem value={2}>Crayond1</MenuItem>
                    <MenuItem value={3}>Crayond2</MenuItem>
                  </CustomSelectField>
                </Grid>
                <Grid item lg={2} md={2} sm={12} xs={12} pr={1}>
                  <Typography variant="label">Revenue Type</Typography>
                  <CustomSelectField fullWidth>
                    <MenuItem value={1}>Crayond</MenuItem>
                    <MenuItem value={2}>Crayond1</MenuItem>
                    <MenuItem value={3}>Crayond2</MenuItem>
                  </CustomSelectField>
                </Grid>
                <Grid item lg={2} md={2} sm={12} xs={12} pr={1}>
                  <Typography variant="label">Measurement Unit</Typography>
                  <CustomSelectField fullWidth>
                    <MenuItem value={1}>Crayond</MenuItem>
                    <MenuItem value={2}>Crayond1</MenuItem>
                    <MenuItem value={3}>Crayond2</MenuItem>
                  </CustomSelectField>
                </Grid>
                <Grid item lg={2} md={2} sm={12} xs={12} pr={1}>
                  <Typography variant="label">Carpet Area</Typography>
                  <CustomTextField defaultValue="Carpet Area" required fullWidth />
                </Grid>
                <Grid item lg={2} md={2} sm={12} xs={12} pr={1}>
                  <Typography variant="label">Total Area</Typography>
                  <CustomTextField defaultValue="Total Area" required fullWidth />
                </Grid>
              </Grid>
              <Grid container>
                <Grid item lg={12}>
                  <Grid container p={1} pl={2} pb={1.5} pr={5}>
                    <Grid item lg={2} md={4} sm={12} xs={12} pr={1}>
                      <Typography variant="label">Year Built</Typography>
                      <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DatePicker
                          value={value}
                          onChange={(newValue) => {
                            setValue(newValue);
                          }}
                          renderInput={(params) => (
                            <CustomTextField fullWidth {...params} />
                          )}
                        />
                      </LocalizationProvider>
                    </Grid>
                    <Grid item lg={2} md={4} sm={12} xs={12} pr={1}>
                      <Typography variant="label">Handover Date</Typography>
                      <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DatePicker
                          value={value}
                          onChange={(newValue) => {
                            setValue(newValue);
                          }}
                          renderInput={(params) => (
                            <CustomTextField fullWidth {...params} />
                          )}
                        />
                      </LocalizationProvider>
                    </Grid>
                    <Grid
                      item
                      lg={3.5}
                      md={4}
                      sm={12}
                      xs={12}
                      pr={1}
                      alignSelf="center"
                    >
                      <Typography variant="label" sx={{ display: "block" }}>
                        Public Listing
                      </Typography>
                      <CustomButton
                        variant="outlined"
                        sx={[{ mr: 0.5 }, active? grey.toggle_active:grey.toggle]}
                        onClick={handleClick}
                      >
                        None
                      </CustomButton>
                      <CustomButton
                        variant="outlined"
                        sx={[{ mr: 0.5 }, active1? grey.toggle_active:grey.toggle]}
                        onClick={handle}
                      >
                        Private
                      </CustomButton>
                      <CustomButton
                        onClick={handleClick2}
                        variant="outlined"
                        sx={[{ mr: 0.5 }, active2? grey.toggle_active:grey.toggle]}
                      >
                        Public
                      </CustomButton>
                    </Grid>
                    <Grid
                      item
                      lg={1.5}
                      md={4}
                      sm={12}
                      xs={12}
                      pr={1}
                      alignSelf="center"
                    >
                      <Typography variant="label" sx={{ display: "block" }}>
                        Pets Allowed
                      </Typography>
                      <Checkbox />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Card>
          </Paper>
        </Grid>
      </Grid>
      {/* ADDRESS */}
      <Grid container spacing={2} ml={0.5} mt={2} pr={5}>
        <Grid item lg={12}>
          <Paper elevation={0} sx={{ borderRadius: 8 }}>
            <Card sx={{ boxShadow: 0 }}>
              <Grid container p={2}>
                <Grid item lg={12}>
                  <Typography variant="overline">Address</Typography>
                </Grid>
                <Grid item lg={5} md={6} sm={12} xs={12} pr={2}>
                  <iframe
                    title="map"
                    src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15552.94722563146!2d80.2655737!3d12.956693249999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1668752766470!5m2!1sen!2sin"
                    width="100%"
                    height="250"
                    allowfullscreen=""
                    loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade"
                  ></iframe>
                </Grid>
                <Grid item lg={7} md={6} sm={12} xs={12}>
                  <Grid container>
                    <Grid item lg={2} md={6} sm={12} xs={12} pr={1}>
                      <Typography variant="label">Door Number</Typography>
                      <CustomTextField defaultValue="Door Number" required fullWidth />
                    </Grid>
                    <Grid item lg={5} md={6} sm={12} xs={12} pr={1}>
                      <Typography variant="label">Address Line 1</Typography>
                      <CustomTextField  defaultValue="Address Line 1" required fullWidth />
                    </Grid>
                    <Grid item lg={5} md={6} sm={12} xs={12} pr={1}>
                      <Typography variant="label">Address Line 2</Typography>
                      <CustomTextField defaultValue="Address Line 2" required fullWidth />
                    </Grid>
                  </Grid>
                  <Grid container mt={1}>
                    <Grid item lg={3} md={6} sm={12} xs={12} pr={1}>
                      <Typography variant="label">Landmark</Typography>
                      <CustomTextField defaultValue="Landmark" required fullWidth />
                    </Grid>
                    <Grid item lg={3} md={6} sm={12} xs={12} pr={1}>
                      <Typography variant="label">Area</Typography>
                      <CustomSelectField fullWidth>
                        <MenuItem value={1}>Crayond</MenuItem>
                        <MenuItem value={2}>Crayond1</MenuItem>
                        <MenuItem value={3}>Crayond2</MenuItem>
                      </CustomSelectField>
                    </Grid>
                    <Grid item lg={3} md={6} sm={12} xs={12} pr={1}>
                      <Typography variant="label">City</Typography>
                      <CustomSelectField fullWidth>
                        <MenuItem value={1}>Crayond</MenuItem>
                        <MenuItem value={2}>Crayond1</MenuItem>
                        <MenuItem value={3}>Crayond2</MenuItem>
                      </CustomSelectField>
                    </Grid>
                    <Grid item lg={3} md={6} sm={12} xs={12} pr={1}>
                      <Typography variant="label">State</Typography>
                      <CustomSelectField fullWidth>
                        <MenuItem value={1}>Crayond</MenuItem>
                        <MenuItem value={2}>Crayond1</MenuItem>
                        <MenuItem value={3}>Crayond2</MenuItem>
                      </CustomSelectField>
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item lg={3} md={6} sm={12} xs={12} pr={1}>
                      <Typography variant="label">Country</Typography>
                      <CustomSelectField fullWidth>
                        <MenuItem value={1}>Crayond</MenuItem>
                        <MenuItem value={2}>Crayond1</MenuItem>
                        <MenuItem value={3}>Crayond2</MenuItem>
                      </CustomSelectField>
                    </Grid>
                    <Grid item lg={3} md={6} sm={12} xs={12} pr={1}>
                      <Typography variant="label">Pincode</Typography>
                      <CustomTextField defaultValue="Pincode" required fullWidth />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Card>
          </Paper>
        </Grid>
      </Grid>
      {/* CONTACTS */}
      <Grid container spacing={2} ml={0.5} mt={2} pr={5}>
        <Grid item lg={12}>
          <Paper elevation={0} sx={{ borderRadius: 8 }}>
            <Card sx={{ boxShadow: 0 }}>
              <Grid container p={2}>
                <Grid item lg={12}>
                  <Typography variant="overline">
                    Contact & Other Information
                  </Typography>
                </Grid>
                <Grid item lg={2} md={3} sm={12} xs={12} pr={1}>
                  <Typography variant="label">Business Phone</Typography>
                  <CustomTextField defaultValue="1234" required fullWidth type="number" />
                </Grid>
                <Grid item lg={2} md={3} sm={12} xs={12} pr={1}>
                  <Typography variant="label">Mobile Phone</Typography>
                  <CustomTextField defaultValue="1234" required fullWidth type="number"/>
                </Grid>
                <Grid item lg={4} md={3} sm={12} xs={12} pr={1}>
                  <Typography variant="label">Website</Typography>
                  <CustomTextField defaultValue="http://localhost:3000/layout/newProperty" required fullWidth type="url"/>
                </Grid>
                <Grid item lg={4} md={3} sm={12} xs={12} pr={1}>
                  <Typography variant="label">Email Adress</Typography>
                  <CustomTextField defaultValue="Website@gmail.com" required fullWidth type="email" />
                </Grid>
              </Grid>
            </Card>
          </Paper>
        </Grid>
      </Grid>
      {/* Footer */}
      <Grid container mt={5}>
        <Grid item lg={12} md={12} sm={12} xs={12}>
          <Paper elevation={0}>
            <Box p={1.5} mr={2} textAlign="right">
              <CustomButton variant="outline" sx={{ marginRight: 1 }}>
                Cancel
              </CustomButton>
              <CustomButton variant="contained" sx={{ color: "white" }} onClick={validate}>
                Submit
              </CustomButton>
            </Box>
          </Paper>
        </Grid>
      </Grid>
    </Box>
  );
}
